# Nabenik's Enterprise Java basic test

Hi and welcome to this test. As many technical interviews, main test objective is to establish your actual Enterprise coding skills, being:

* Java knowledge
* JavaScript knowledge
* DDD knowledge
* General toolkits, SDK's and other usages
* Jakarta EE general skills

To complete this test, please create a fork of this repository, fix the source code if required, add your answers to this readme file, commit the solutions/answers to YOUR copy and create a pull request to the original repo.

This document is structured using [GitHub Markdown Flavor](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#code).

## General questions

1. How to answer these questions?

> Like this

Or maybe with code

```kotlin
fun hello() = "world"
```

2. Please describe briefly the main purpose for the following Jakarta EE specs, also add in your answer http links to actual implementations

- JSON-P
  >JSON-P is an API that can move data, it can cross domains. It’s very similar to JSON, actually they’ve the same structure, only changing its container.
- CDI
  > As it name says ( context and dependency injection) it is the default dependencies injections to javaEE
- JPA
  >The Java persistence api is a tool developed for javaee, over this tool, a lot of frameworks were developed, for example hibernate.
- JAX-RS
  >This tool helps to create web services to use in REST applications,


3. Which of the following is/are not an application server?

    >WebSphere

4. In your opinion what's the main benefit of moving from Java 8 to Java 11
   > The support from Oracle, java 8 doesn’t have more support.

5. In your opinion what's the main benefit of using TypeScript over JavaScript
   >Dependency Injection and the scalability of the code.

6. What's the main difference between OpenJDK and HotSpot
   >The difference is the money, one is open source, and the other is private.

7. If no database is configured? Will you be able to run this project? Why?
   >Yes, I will be able to run the project without a DB configured. The project can run due to the javaEE architecture, it makes an independent DB environment.

## Development tasks

Please also include screenshots on every task. You don't need to execute every task to submit your pull request but feel free to do it :).

0. (easy) Show your terminal demonstrating your installation of OpenJDK 11 and  NodeJS 16

   ![Screenshot](Screenshots/0-java-node.png)

1. (easy) Build this project on a regular CLI over OpenJDK 11

   ![Screenshot](Screenshots/1-mvn.png)

   ![Screenshot](Screenshots/1-mvn2.png)

2. (easy) Run this project using an IDE/Editor of your choice

   ![Screenshot](Screenshots/2-ide.png)

3. (medium) This project has been created using Java EE APIs, please identify at least four APIs, bump it to Java EE 8 and Java 11, later run it over regular Payara Application Server

   ![Screenshot](Screenshots/3.-payara.png)

4. (medium) Execute the movie endpoint operations using a client -e.g. Postman-, if needed please also check/fix the code to be REST compliant
   
    ![Screenshot](Screenshots/4-postman.png)

5. (medium) Write an SPA application using Angular, the application should be a basic CRUD that uses Movie operations, upload this application to a new Bitbucket repo and include the link as answer
   >https://bitbucket.org/jpmazate/angularnabeniktest/src/master/

   ![Screenshot](Screenshots/5-Angular.png)

6. (hard) Please identify the Integration Test for `MovieRepository`, after that implement each of the non-included CRUD methods

   ![Screenshot](Screenshots/6-crud.png)

   ![Screenshot](Screenshots/6-crud2.png)

7. (hard) Based on `MovieRepository` Integration Test, please create an integration test using [RestAssured](https://rest-assured.io/) and `MovieController`

   ![Screenshot](Screenshots/7-rest-assured.png)

8.(hard) Please write the Repository and Controller for Actor model, after that implement each of the CRUD methods using an Integration Test
    ![Screenshot](Screenshots/8-TEST.png)
    ![Screenshot](Screenshots/8-test2.png)
    ![Screenshot](Screenshots/8-test3.png)
    ![Screenshot](Screenshots/8-test4.png)


9.(nightmare) This source code includes only Java EE APIs, hence it's possible to port it to [Oracle Helidon](https://helidon.io/). Do it and don't port Integration Tests


