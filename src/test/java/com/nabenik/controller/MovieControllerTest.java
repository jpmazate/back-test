package com.nabenik.controller;

import io.restassured.http.ContentType;
import net.minidev.json.JSONObject;
import org.junit.Test;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

import static org.junit.Assert.*;

public class MovieControllerTest {

    @Test
    public void createTest() {
        String url = "/back-test/rest/movies";
        JSONObject params= new JSONObject();
        params.put("title","Titulo");
        params.put("year","2010");
        params.put("duration","5hr");
        given()
                .accept("application/json")
                .contentType(ContentType.JSON)
                .and()
                .body(params).
                when()
                .post(url)
                .then()
                .statusCode(201);
    }
    @Test
    public void updateTest() {
        String url = "/back-test/rest/movies";
        JSONObject params= new JSONObject();
        params.put("title","Titulo");
        params.put("year","2010");
        params.put("duration","5hr");
        given()
                .accept("application/json")
                .contentType(ContentType.JSON)
                .and()
                .body(params).
                when()
                .put(url+"/"+5)
                .then()
                .statusCode(201);
    }
    @Test
    public void deleteTest() {
        given()
                .contentType(ContentType.JSON)
                .when().delete("/back-test/rest/movies/10").
                then().statusCode(500);
    }


}
