package com.nabenik.repository;

import com.nabenik.model.Actor;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(Arquillian.class)
public class ActorRepositoryTest {

    @Inject
    ActorRepository actorRepository;

    @Deployment
    public static JavaArchive createDeployment() {
        return ShrinkWrap.create(JavaArchive.class)
                .addClass(ActorRepository.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    private Actor createDefaultActor(){
        Actor actor = new Actor();
        actor.setBirthday("10/03/2000");
        actor.setName("Luis Rojas");
        actor.setCountry("Mexico");
        return actor;
    }

    @Test
    public void create() {
        Actor actor = createDefaultActor();
        actorRepository.create(actor);
        assertNotNull(actor.getActorId());
    }

    @Test
    public void update() {
        Actor actor = createDefaultActor();
        actorRepository.create(actor);
        actor.setName("Nuevo");
        actorRepository.update(actor);
        assertEquals(actor.getName(),"Nuevo");
    }

    @Test
    public void delete() {
        Actor actor =createDefaultActor();
        actorRepository.delete(actor);
        assertNull(actor.getActorId());
    }

    @Test
    public void findById() {
        Actor actor = createDefaultActor();
        actorRepository.create(actor);
        Actor searchedActor = actorRepository.findById(actor.getActorId());
        assertNotNull(searchedActor.getActorId());
    }

    @Test
    public void listAll() {
        List<Actor> movieList = actorRepository.listAll();
        assertEquals(0,movieList.size()); // no deberia de existir ninguno
    }
}
