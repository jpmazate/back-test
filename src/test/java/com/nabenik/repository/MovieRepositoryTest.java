package com.nabenik.repository;

import com.nabenik.model.Actor;
import com.nabenik.model.Movie;
import com.nabenik.rest.JAXRSConfiguration;
import com.nabenik.util.EntityManagerProducer;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(Arquillian.class)
public class MovieRepositoryTest {

    @Inject
    MovieRepository movieRepository;

    @Deployment
    public static WebArchive createDeployment(){
        WebArchive war = ShrinkWrap.create(WebArchive.class)
                .addClass(Movie.class)
                .addClass(Actor.class)
                .addClass(EntityManagerProducer.class)
                .addClass(MovieRepository.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsResource("META-INF/persistence.xml");

        System.out.println(war.toString(true));

        return war;
    }

    @Test
    public void create() {
        Movie movie = new Movie("El silencio de Jimmy", "2014", "4 años");
        movieRepository.create(movie);


        System.out.println("Movie Id " + movie.getMovieId());

        assertNotNull(movie.getMovieId());
    }
    @Test
    public void listAll() {
        List<Movie> movieList = movieRepository.listAll("");
        assertEquals(0,movieList.size());
    }
    @Test
    public void findById() {
        Movie movie = new Movie("El silencio de Jimmy", "2014", "4 años");
        movieRepository.create(movie);
        Movie movieBuscada = movieRepository.findById(movie.getMovieId());
        assertEquals(movieBuscada, movie);
    }
    @Test
    public void update() {
        Movie movieToUpdate = new Movie("El silencio de Jimmy", "2014", "4 años");
        movieRepository.create(movieToUpdate);
        movieToUpdate.setTitle("Cambiando el titulo");
        movieRepository.update(movieToUpdate);
        assertNotNull(movieToUpdate.getMovieId());
    }
    @Test
    public void delete() {
        Movie movieToDelete = new Movie("El silencio de Jimmy", "2014", "4 años");
        movieRepository.delete(movieToDelete);
        assertNull(movieToDelete.getMovieId());
    }



}
