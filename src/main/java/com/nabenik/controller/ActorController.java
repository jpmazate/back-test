package com.nabenik.controller;
import com.nabenik.model.Actor;
import com.nabenik.repository.ActorRepository;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/actores")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ActorController {
    @Inject
    ActorRepository actorRepository;
    @GET
    public List<Actor> listAll() {

        return actorRepository.listAll();
    }
    @GET
    @Path("/{id}")
    public Actor findById(@PathParam("id") Long id) {

        return actorRepository.findById(id);
    }
    @POST
    public Response create(Actor actor) {
        actorRepository.create(actor);
        return Response.ok("Se guardo correctamente").build();
    }
    @PUT
    @Path("/{id}")
    public Response update(@PathParam("id") Long id, Actor actor) {
        actorRepository.update(actor);
        return Response.ok("Se actualizo correctamente").build();
    }
    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") Long id) {
        Actor actor = actorRepository.findById(id);
        actorRepository.delete(actor);
        return Response.ok().build();
    }
}
